<?php
/*
 * Routes file.
 * In this file you can configure the routes your app should use.
 * By default we create a default route home.
 * 
 */
return [
    'home' => [
        'url' => '/',
        'controller' => \Viridis\Controller\HomeController::class,
        'action' => 'index',
        'method' => 'GET'
    ],
    'users' => [
        'url' => '/users',
        'controller' => \Viridis\Controller\UsersController::class,
        'action' => 'index',
        'method' => 'GET'
    ],
    '404' => [
        'url' => '/404',
        'controller' => \Viridis\Controller\HomeController::class,
        'action' => 'pageNotFound',
        'method' => 'GET'
    ],
    'parameters' => [
        'url' => '/home/{testParam}/view/{viewId}/{test1}',
        'controller' => \Viridis\Controller\HomeController::class,
        'action' => 'parametersTest',
        'method' => 'GET'
    ]
];
