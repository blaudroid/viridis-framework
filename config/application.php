<?php
return [
    'app' => [
        'name' => 'Viridis Framework',
        'env' => 'dev',
        // Change dev to prod if you want to use prod DB.
        'db' => 'db_dev',
        'url' => 'http://viridis.test:8081'
    ],
    'db_prod' => [
        'name' => 'viridis_db',
        'user' => 'viridis_prod',
        'pass' => '',
        'host' => 'localhost',
        'port' => 3306
    ],
    'db_dev' => [
        'name' => 'viridis_db_dev',
        'user' => 'viridis_dev',
        'pass' => '',
        'host' => 'localhost',
        'port' => 3306
    ]
];
