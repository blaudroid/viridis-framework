<?php
/**
 * This is the entry point of the app.
 * @author Gabriel Nastase <gabriel.nastase@vonaffenfels.de>
 * @copyright (c) 2021 vonAffenfels GmbH
 * @package viridis
 */
require '../vendor/autoload.php';

use DI\Container;
use Viridis\Application;
use Viridis\Config\Config;

$container = new Container();
$config = $container->get(Config::class);

//If you don't want the warnings to be shown change the application.php config to prod.
if($config->get('app.env') === "dev") {
    error_reporting(E_ALL);
    ini_set('display_errors',1);
    ini_set('error_reporting', E_ALL);
    ini_set('display_startup_errors',1);
    error_reporting(-1);
}

//Creating a new Application Instance.
$app = new Application($container);
$app->run();

