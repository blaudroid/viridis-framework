<?php

namespace Viridis;

use Exception;

class ResourceManager
{

    /**
     * @var string
     */
    protected string $routesPath = __DIR__ . '/../resources/routes/';

    /**
     * @var string
     */
    protected string $routesFile = 'routes.php';

    /**
     * @var string
     */
    protected string $viewsPath = __DIR__ . '/../resources/views/';

    /**
     * @var string
     * Disabled.
     */
//    protected string $twigCachePath = __DIR__ . '/../cache/twig/';

    /**
     * @var string
     */
    protected string $configPath = __DIR__ . '/../config/';

    /**
     * @var string
     */
    protected string $configFile = 'application.php';

    /**
     * @return string
     */
    public function getRoutesPath(): string
    {
        return $this->routesPath;
    }

    /**
     * @return string
     */
    public function getRoutesFile(): string
    {
        return $this->routesFile;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getRoutes(): array
    {
        $path = $this->getRoutesPath() . $this->getRoutesFile();

        if(!file_exists($path))
        {
            throw new Exception("Missing file: {$this->getRoutesFile()}");
        }

        return require($path);
    }

    /**
     * @return string
     */
    public function getViewsPath(): string
    {
        return $this->viewsPath;
    }

    /**
     * @return string
     * Disabled.
     */
//    public function getTwigCachePath(): string
//    {
//        return $this->twigCachePath;
//    }

    /**
     * @return string
     */
    public function getConfigFile(): string
    {
        return $this->configFile;
    }

    /**
     * @return string
     */
    public function getConfigPath(): string
    {
        return $this->configPath;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getRawConfig()
    {
        if(!file_exists($this->configPath . $this->configFile)) {
            throw new Exception("Missing file: {$this->configFile}.");
        }

        return require($this->configPath . $this->configFile);
    }
}
