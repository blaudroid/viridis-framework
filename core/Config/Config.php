<?php
namespace Viridis\Config;

use Viridis\ResourceManager;

/**
* This is the Config model.
* @author Gabriel Nastase <gabriel.nastase@vonaffenfels.de>
* @copyright (c) 2021 vonAffenfels GmbH
* @package viridis
*/
class Config implements ConfigInterface
{

    /**
     * @var ResourceManager
     */
    protected ResourceManager $resourceManager;

    /**
     * @var array
     */
    protected array $rawConfig;

    /**
     * Config constructor.
     * @param ResourceManager $resourceManager
     * @throws \Exception
     */
    public function __construct(ResourceManager $resourceManager)
    {
        $this->resourceManager = $resourceManager;
        $this->rawConfig = $this->resourceManager->getRawConfig();
    }


    public function get(string $matrix)
    {
        return $this->search($this->rawConfig, $matrix);
    }

    /**
     * @param array $toSearch
     * @param string $matrix
     * @return mixed|null
     * @throws \Exception
     */
    protected function search(array $toSearch, string $matrix)
    {
        $searchKeys = explode('.', $matrix);
        $currentSearchKey = array_shift($searchKeys);

        if(empty($searchKeys)) {
            if(!is_array($toSearch)) {
                return null;
            }
            return $toSearch[$currentSearchKey];
        }

        try {
            return $this->search($toSearch[$currentSearchKey], join('.', $searchKeys));
        } catch (\TypeError $e)
        {
            return $e->getTrace() . "\nAn error has occured while searching for config key \"{$currentSearchKey}\" in {$this->resourceManager->getConfigFile()}.";
        }
    }
}
