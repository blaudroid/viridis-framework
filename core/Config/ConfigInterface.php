<?php

namespace Viridis\Config;

/**
* This is the ConfigInterface.
* @author Gabriel Nastase <gabriel.nastase@vonaffenfels.de>
* @copyright (c) 2021 vonAffenfels GmbH
* @package viridis
*/
interface ConfigInterface
{
    /**
     * @param string $matrix
     * @return mixed
     */
    public function get(string $matrix);
}
