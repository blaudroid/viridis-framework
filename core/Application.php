<?php

namespace Viridis;

use DI\Container;
use Exception;
use Viridis\Http\Router\Router;
use Viridis\Http\Request\Request;

/**
* This is the main class that will be called when accessing the webpage.
* @author Gabriel Nastase <gabriel.nastase@vonaffenfels.de>
* @copyright (c) 2021 vonAffenfels GmbH
* @package viridis
*/
class Application
{
    /**
     * @var Router
     */
    private Router $router;

    /**
     * @var Request
     */
    private Request $request;

    /**
     * @var Container
     */
    protected Container $container;

    /**
     * Application constructor.
     * @param Container $container
     * @throws Exception
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->request = $container->get(Request::class);
        $this->router = $container->get(Router::class);
    }


    /**
     * Run the application.
     * @throws Exception
     */
    public function run() {
        //First we check if the Request is HTTP.
        //If not, then we are going to run the CLI App.
        if($this->request->isHttpRequest())
        {
            $this->runHttpApp($this->request);
        }

        //@todo create CLI app.
        $this->runCliApp();
    }

    /**
     * @param Request $request
     * @throws Exception
     */
    protected function runHttpApp(Request $request)
    {
        $this->router->route($request);
    }

    protected function runCliApp()
    {
        //@todo implement cli app for the future.
    }
}
