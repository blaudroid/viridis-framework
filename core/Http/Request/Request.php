<?php

namespace Viridis\Http\Request;

/**
* This class takes care of the requests.
* @author Gabriel Nastase <gabriel.nastase@vonaffenfels.de>
* @copyright (c) 2021 vonAffenfels GmbH
* @package viridis
*/
class Request
{
    /**
     * @var string
     */
    protected $method;

    /**
     * @var string
     */
    protected $url;

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->method = $_SERVER['REQUEST_METHOD'] ?? NULL;
        $this->url = $_SERVER['REQUEST_URI'] ?? NULL;
    }

    /**
     * @return mixed|string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return mixed|string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return bool
     */
    public function isHttpRequest(): bool
    {
        if($this->method) {
            return true;
        }
        return false;
    }

    /**
     * @return array|string[]
     */
    public function getRequest(): array
    {
        return [
            'url' => $this->getUrl(),
            'method' => $this->getMethod()
        ];
    }
}
