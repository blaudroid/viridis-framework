<?php

namespace Viridis\Http\Router;

use DI\Container;
use Exception;
use http\Exception\RuntimeException;
use Viridis\Config\Config;
use Viridis\Http\Request\Request;
use Viridis\ResourceManager;

/**
* This is the router.
* @author Gabriel Nastase <gabriel.nastase@vonaffenfels.de>
* @copyright (c) 2021 vonAffenfels GmbH
* @package viridis
*/
class Router
{
    /**
     * @var Request
     */
    protected Request $request;

    /**
     * @var ResourceManager
     */
    protected ResourceManager $resourceManager;

    /**
     * @var Container
     */
    protected Container $container;

    /**
     * @var Config
     */
    protected Config $config;

    /**
     * Router constructor.
     * @param Request $request
     * @param ResourceManager $resourceManager
     * @param Container $container
     * @param Config $config
     */
    public function __construct(Request $request, ResourceManager $resourceManager, Container $container, Config $config)
    {
        $this->request = $request;
        $this->resourceManager = $resourceManager;
        $this->container = $container;
        $this->config = $config;
    }

    /**
     * @throws Exception
     */
    public function route(Request $request)
    {
        if(!$request) {
            throw new Exception('Route needs a request.');
        }

        $route = $this->buildRoute($request);
        $this->dispatch($route);
    }

    /**
     * @param Request $request
     * @return Route
     * @throws Exception
     */
    protected function buildRoute(Request $request): Route
    {
        $url = $request->getUrl();

        $routeFromFile = $this->searchForRouteInFile($url);

        $route = new Route();
        $route->setName($routeFromFile['name']);
        $route->setAction($routeFromFile['action']);
        $route->setController($routeFromFile['controller']);
        $route->setMethod($routeFromFile['method']);
        $route->setParameters($routeFromFile['parameters']);

        return $route;
    }

    /**
     * Method used to dispatch the route.
     * @param Route $route
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    protected function dispatch(Route $route)
    {
        $controller = $this->container->get($route->getController());
        $this->container->call([$controller, $route->getAction()], ['parameters' => $route->getParameters()]);
    }

    /**
     * @param string $url
     * @throws Exception
     */
    public function redirect(string $url)
    {
        if(!$url) {
            throw new Exception('URL cannot be empty !');
        }

        $urlToRedirect = $this->config->get('app.url') . $url;
        header("Location: {$urlToRedirect}");
    }

    /**
     * @param string $url
     * @return array
     * @throws Exception
     */
    protected function searchForRouteInFile(string $url): array
    {
        $fileRoutes = $this->resourceManager->getRoutes();
        $routesPhysicalFile = $this->resourceManager->getRoutesFile();

        if(!count($fileRoutes)) {
            throw new Exception("File routes {$routesPhysicalFile} does not contain any routes!");
        }

        foreach($fileRoutes as $fileRouteName => $fileRouteValues) {
            //Checking if the route that is written has the values we need.
            if(!count($fileRouteValues)) {
                throw new Exception("Empty route \"$fileRouteName\" in {$routesPhysicalFile} !");
            }

            //Checking if the route has the url property and is not empty or null.
            if(
                !array_key_exists('url', $fileRouteValues) &&
                is_null($fileRouteValues['url']) ||
                empty(trim($fileRouteValues['url']))
            ) {
                throw new Exception("URL entry for \"$fileRouteName\" in {$routesPhysicalFile} is empty or missing.");
            }

            //Checking if the route has controller property and is not empty or null.
            if(
                !array_key_exists('controller', $fileRouteValues) &&
                is_null($fileRouteValues['controller']) ||
                empty(trim($fileRouteValues['controller']))
            ) {
                throw new Exception("Controller entry for \"$fileRouteName\" in {$routesPhysicalFile} is empty or missing.");
            }

            //Checking if the route has action url property and is not empty or null.
            if(
                !array_key_exists('action', $fileRouteValues) &&
                is_null($fileRouteValues['action']) ||
                empty(trim($fileRouteValues['action']))
            ) {
                throw new Exception("Controller action entry for \"$fileRouteName\" in {$routesPhysicalFile} is empty or missing.");
            }

            //Checking if the route has method url property and is not empty or null.
            if(!array_key_exists('method', $fileRouteValues) &&
                is_null($fileRouteValues['method']) ||
                empty(trim($fileRouteValues['method']))
            ) {
                throw new Exception("Method entry for \"$fileRouteName\" in {$routesPhysicalFile} is empty or missing.");
            }

            $extraOptions = [
                'name' => $fileRouteName,
                'parameters' => []
            ];

            if($url !== $fileRouteValues['url'])
            {
                $parameters = $this->checkUrlForParameters($url, $fileRouteValues['url']);
                $extraOptions['parameters'] = $parameters;

                if(!$parameters) {
                    continue;
                }

                return array_merge($fileRouteValues, $extraOptions);
            }

            return array_merge($fileRouteValues, $extraOptions);
        }

        throw new Exception("Could not find route for {$url} in {$routesPhysicalFile}");
    }

    /**
     * @param string $url
     * @param string $fileRouteUrl
     * @return array
     * @throws Exception
     */
    protected function checkUrlForParameters(string $url, string $fileRouteUrl): array
    {
        $fileRouteUrlParts = $this->filterRouteParts(explode('/', $fileRouteUrl));
        $urlParts = $this->filterRouteParts(explode('/', $url));

        if(count($fileRouteUrlParts) !== count($urlParts)) {
            return [];
        }

        $parameters = [];

        foreach($fileRouteUrlParts as $fileRouteUrlKey => $fileRouteUrlPart)
        {
            foreach($urlParts as $urlPartKey => $urlPart)
            {
                if($fileRouteUrlKey === $urlPartKey) {
                    $openingBracket = strpos($fileRouteUrlPart, '{');

                    if($openingBracket === false) {
                        continue;
                    }

                    $parameterOpened = substr($fileRouteUrlPart, $openingBracket + 1);
                    $parameterClosed = strstr($parameterOpened, '}', true);
                    $parameters[$parameterClosed] = $urlPart;
                }
            }
        }

        return $parameters;
    }

    /**
     * @param array $parts
     * @return array
     */
    protected function filterRouteParts(array $parts): array
    {
        $cleanParts = [];

        foreach($parts as $part)
        {
            if(!empty($part)) {
                $cleanParts[] = $part;
            }
        }

        return $cleanParts;
    }
}
