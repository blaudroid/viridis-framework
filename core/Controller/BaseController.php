<?php
namespace Viridis\Controller;

use DI\Container;
use DI\DependencyException;
use DI\NotFoundException;
use Viridis\Http\Request\Request;
use Viridis\Http\Router\Router;
use Viridis\ResourceManager;
use Viridis\View\View;

class BaseController
{
    /**
     * @var Container
     */
    public Container $container;

    /**
     * @var View
     */
    public View $view;

    /**
     * @var Request
     */
    public Request $request;

    /**
     * @var Router|mixed
     */
    public Router $router;

    /**
     * BaseController constructor.
     * @throws DependencyException
     * @throws NotFoundException
     */
    public function __construct()
    {
        $this->container = new Container();
        $this->view = new View($this->container->get(ResourceManager::class));
        $this->request = new Request();
        $this->router = $this->container->get(Router::class);
    }
}
