<?php
namespace Viridis\Controller;

/**
 * This is the home controller.
 * This controller will be created upon installing the framework.
 * @author Gabriel Nastase <gabriel.nastase@vonaffenfels.de>
 * @copyright (c) 2021 vonAffenfels GmbH
 * @package viridis
 */
class UsersController extends BaseController
{
    public function index()
    {
        //@todo implement index function
        echo "Hello Users!";
    }
}