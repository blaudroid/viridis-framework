<?php
namespace Viridis\Controller;

use Viridis\Http\Request\Request;

/**
* This is the home controller.
* This controller will be created upon installing the framework.
* @author Gabriel Nastase <gabriel.nastase@vonaffenfels.de>
* @copyright (c) 2021 vonAffenfels GmbH
* @package viridis
*/
class HomeController extends BaseController
{
    public function index()
    {
//        $this->router->redirect('/404');
        $this->view->render('home.index', ['testVariable' => 'home controller']);
    }


    public function pageNotFound()
    {
        $this->view->render('home.index', ['testVariable' => 'page not found']);
    }

    public function parametersTest($parameters)
    {
        $this->view->render('home.index', ['testVariable' => $parameters]);
    }
}
