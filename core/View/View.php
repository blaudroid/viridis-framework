<?php
namespace Viridis\View;

use Exception;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;
use Viridis\ResourceManager;

/**
* This is the View main class.
* @author Gabriel Nastase <gabriel.nastase@vonaffenfels.de>
* @copyright (c) 2021 vonAffenfels GmbH
* @package viridis
*/
class View {

    /**
     * @var ResourceManager
     */
    protected ResourceManager $resourceManager;

    /**
     * @var FilesystemLoader
     */
    protected FilesystemLoader $loader;

    /**
     * @var Environment
     */
    protected Environment $twig;

    /**
     * View constructor.
     * @param ResourceManager $resourceManager
     */
    public function __construct(ResourceManager $resourceManager)
    {
        $this->resourceManager = $resourceManager;
        $this->loader = new FilesystemLoader($this->resourceManager->getViewsPath());
        $this->twig = new Environment($this->loader);
    }

    /**
     * @param string $matrix
     * @param array $variables
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function render(string $matrix, array $variables = [])
    {
        $toRenderFilePath = $this->makeRealPathFromMatrix($matrix);
        echo $this->twig->render($toRenderFilePath, $variables);
    }

    /**
     * @param string $matrix
     * @return string
     * @throws Exception
     */
    protected function makeRealPathFromMatrix(string $matrix): string
    {
        $templatePathParts = explode('.', $matrix);

        if(!$matrix) {
            throw new Exception("Missing or empty argument 'matrix' !");
        }

        $viewTemplatePath = null;

        foreach($templatePathParts as $tplPart)
        {
            if($tplPart === end($templatePathParts)) {
                $viewTemplatePath .= "{$tplPart}.html.twig";
            } else {
                $viewTemplatePath .= "/{$tplPart}/";
            }
        }

        return $viewTemplatePath;
    }
}
